public class Avionmilitar extends Avion{
    /**
     * atributos
     */

    private String misiles;


    /**
     * constructor
     */

    public Avionmilitar(String color, double tamanio){
        super(color, tamanio);
    }

    
    /**
     * metodos
     */

    private void apuntando() {
        System.out.println("apuntando al objetivo.......");
    }

    private void disparar() {
        System.out.println("disparando misil............");
    }

    private void recargando() {
        System.out.println("recargando misiles..........");
    }
}
