public class App {
    public static void main(String[] args) throws Exception {
        //construir un avion de carga
        Avioncarga objAvioncarga = new Avioncarga("gris", 150.5);
        objAvioncarga.cargar();
        objAvioncarga.despegar();
        Avionpasajeros objAvionpasajeros = new Avionpasajeros("blanco", 120.6);
        objAvionpasajeros.servir();
        Avionmilitar objAvionmilitar = new Avionmilitar("negro", 200.2);
        objAvionmilitar.apuntando();
        objAvionmilitar.disparar();
        objAvionmilitar.recargando();
    }
}
